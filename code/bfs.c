#include<stdio.h>
#include<stdlib.h>
int n,G[30][30],visited[30],q[10],v,rear=1,front=0;
void bfs(int v)
{
int i;
visited[v]=1;
for(i=1;i<=n;i++)
if(G[v][i] && !visited[i])
q[++rear]=i;
if(front <=rear)
bfs(q[front++]);
}

void main()
{
int i,j;
printf("*-----------------BFS-----------------*\n");
for(i=1;i<=n;i++)
{
q[i]=0;
visited[i]=1;
}
printf("Enter the number of nodes:");
scanf("%d",&n);
printf("\nEnter the adjacency matrix:\n");
for(i=1;i<=n;i++)
for(j=1;j<=n;j++)
scanf("%d",&G[i][j]);
printf("\nenter the starting vertex:");
scanf("%d",&v);
bfs(v);
printf("\n The node which are reachable are:\n");
for(i=1;i<=n;i++)
if(visited[i])
printf("%d\t",i);
else
printf("%d\t is not reachable\n",i);
}