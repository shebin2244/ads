#include<stdio.h>
#include<stdlib.h>
void prims(int n,int cost[30][30])
{
int visited[20],i,j,ne=1,min,u,v,mincost=0;
for(i=1;i<=n;i++)
visited[i]=0;
printf("\n the edges considered for MST are\n");
visited[1]=1;
while(ne<n)
{
for(i=1,min=999;i<=n;i++)
{
for(j=1;j<=n;j++)
{
if(cost[i][j]<min)
{
if(visited[i]==0)
continue;
else
{
min=cost[i][j];
u=i;
v=j;
}
}
}
}

if(visited[u]==0||visited[v]==0)
{
printf("%d edge(%d,%d)=%d\n",ne++,u,v,min);
mincost=mincost+min;
visited[v]=1;
}
cost[u][v]=cost[v][u]=999;
}
printf("\n cost of computing spanning tree is:%d",mincost);
}

//Main function is
int main()
{
int n,i,j,cost[30][30];
printf("Enter the number of nodes:");
scanf("%d",&n);
printf("\nRead cost matrix\n");
for(i=1;i<=n;i++)
{
for(j=1;j<=n;j++)
{
scanf("%d",&cost[i][j]);
if(cost[i][j]==0)
cost[i][j]=999;
}
}

prims(n,cost);
return 0;
}